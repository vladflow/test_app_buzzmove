module.exports = {
  devtool: 'inline-source-map',
  entry: {
    main: './src/react-app.js'
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loaders: [ 'babel', 'eslint-loader' ]
      },
      { test: /\.json$/, loader: 'file' },
      { test: /\.jpg$/, loader: 'file' },
      { test: /\.scss$/, loaders: [ 'style', 'css', 'sass' ] },
      { test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/, loader: 'url-loader' }
    ]
  },
  progress: true,
  resolve: {
    extensions: ['', '.json', '.js']
  },
  output: {
    path: 'build/',
    filename: 'bundle.js'
  },
  devServer: {
    historyApiFallback: true
  }
};
