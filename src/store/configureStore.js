import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers';
import { reducer as formReducer } from 'redux-form';

let store = null;

const middleWare = [
  thunkMiddleware
];

const devFuncs = [
  window.devToolsExtension ? window.devToolsExtension() : f => f
];

export default function configureStore(initialState) {
  store = compose(
    applyMiddleware(...middleWare),
    ...devFuncs
  )(createStore);

  return store(
    combineReducers({
      rootReducer,
      routing: routerReducer,
      form: formReducer
    }),
    initialState);
}
