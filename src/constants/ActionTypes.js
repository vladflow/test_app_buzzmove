//PAGE DISPATCH ACTIONS
export const ADD_TASK = 'ADD_TASK';
export const EDIT_TASK = 'EDIT_TASK';
export const SAVE_TASKS = 'SAVE_TASKS';
export const REMOVE_TASK = 'REMOVE_TASK';
export const PAGE_FETCHING = 'PAGE_FETCHING';
export const ERROR_PAGE = 'ERROR_PAGE';
export const FILTER_LIST = 'FILTER_LIST';
