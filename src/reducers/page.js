import * as types from '../constants/ActionTypes.js';
import * as pageState from './initialStates/page_state.js';

export default function page(state = pageState.initial, action) {
  switch (action.type) {
    case types.SAVE_TASKS:
      return Object.assign({}, state, {
        tasks: {
          ...action.tasks,
          tags: [ ...new Set(action.tasks.items.map(task => task.tag)) ]
        },
        page_status: {
          ...state.page_status,
          isFetching: false,
          loaded: true
        }
      });
      break;
    case types.EDIT_TASK:
      const task = action.task;
      let tasksItems = state.tasks.items;

      const result = tasksItems.filter(object => object.id === task.id);
      if (result.length === 0) tasksItems.push(task);
      else {
        for (let index in tasksItems) {
          if (tasksItems[index].id === task.id) tasksItems[index] = { ...task };
        }
      }

      return Object.assign({}, state, {
        tasks: {
          ...state.tasks,
          tags: [ ...new Set(state.tasks.items.map(task => task.tag)) ],
          items: [
            ...tasksItems
          ]
        },
        page_status: {
          ...state.page_status,
          isFetching: false,
          loaded: true
        }
      });
      break;
    case types.REMOVE_TASK:
      return Object.assign({}, state, {
        tasks: {
          ...state.tasks,
          items: [
            ...state.tasks.items.filter(task => task.id !== action.taskId)
          ]
        },
        page_status: {
          ...state.page_status
        }
      });
    case types.FILTER_LIST:
      return Object.assign({}, state, {
        tasks: {
          ...state.tasks,
          filterBy: action.tag
        },
        page_status: {
          ...state.page_status
        }
      });
    case types.PAGE_FETCHING:
      return Object.assign({}, state, {
        tasks: {
          ...state.tasks
        },
        page_status: {
          ...state.page_status,
          isFetching: true,
          loaded: false,
        }
      });
    case types.PAGE_FETCHING:
      return Object.assign({}, state, {
        tasks: {
          ...state.tasks
        },
        page_status: {
          loadingError: true,
          errorMessage: action.error,
          isFetching: false,
          loaded: false,
        }
      });
    default:
      return state;
  }
}
