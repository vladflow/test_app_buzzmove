import { combineReducers } from 'redux';

import page from './page.js';

const rootReducer = combineReducers({
  page
})

export default rootReducer;
