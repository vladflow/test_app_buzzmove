export const initial = {
  tasks: {},
  page_status: {
    isFetching: false,
    loaded: false,
    loadingError: false,
    errorMessage: ''
  }
};
