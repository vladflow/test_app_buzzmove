import React from 'react';
let { Component } = React;
import ReactDOM, { render } from 'react-dom';
import {
  Link,
  HistoryLocation,
  IndexRoute,
  Route,
  Router,
  browserHistory
} from 'react-router';

import configureStore from './store/configureStore.js';
import { Provider } from 'react-redux';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

class ClientApp extends Component {
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    )
  }
}

ClientApp.contextTypes = {
  history: React.PropTypes.object
};

import TasksPage from './components/pages/TasksPage';
import NotFound from './components/pages/NotFound';

render((
  <Provider store={store}>
    <Router history={history}>
      <Route name="StoreApp" path="/" component={ClientApp}>
        <IndexRoute component={TasksPage} />
      </Route>
    </Router>
  </Provider>
), document.getElementById('SiteContent'));
