import fetch from 'isomorphic-fetch';
import * as types from '../constants/ActionTypes.js';

export function pageError(error) {
  return {
    type: types.ERROR_PAGE,
    error: error,
  }
}

export function pageFetching() {
  return {
    type: types.PAGE_FETCHING,
  }
}

export function addTask(error) {
  return {
    type: types.ADD_TASK,
    error: error
  }
}

export function editTask(task) {
  return {
    type: types.EDIT_TASK,
    task: task
  }
}

export function removeTask(id) {
  return {
    type: types.REMOVE_TASK,
    taskId: id
  }
}

export function saveTasks(taskJSON) {
  return {
    type: types.SAVE_TASKS,
    tasks: taskJSON
  }
}

export function filterBy(tag = false) {
  return {
    type: types.FILTER_LIST,
    tag: tag
  }
}

export function getTasks() {
  return dispatch => {
    dispatch(pageFetching());
    fetch('/data/tasks.json')
      .then((res) => res.json())
      .then((data) => {
        dispatch(saveTasks(data))
      }).catch(error => {
        dispatch(pageError('There has been a problem with your fetch operation: ' + error.message));
      });
  }
}
