import React, {Component} from 'react';
import '../../../assets/app.scss';

export default class NotFound extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-12 group-item">
              <h2 style={{textAlign: 'center'}}>SORRY, PAGE NOT FOUND</h2>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
