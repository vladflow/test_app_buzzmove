import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as PageActions from '../../actions/PageActions.js';
import PageLoader from '../loaders/PageLoader.js';
import TaskList from '../list/TaskList.js';
import '../../../assets/app.js';
import '../../../assets/app.scss';

class TasksPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.actions.getTasks();
  }

  render() {
    return (
      <div>
        <PageLoader>
          <div className="container">
            <div className="row">
              <div className="col-md-3"></div>
              <div id="TasksContainer" className="col-md-6">
                <TaskList/>
              </div>
              <div className="col-md-3"></div>
            </div>
          </div>
        </PageLoader>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(PageActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TasksPage);
