import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm, SubmissionError, change, touch} from 'redux-form';
import autobind from 'autobind-decorator';
import * as validateRules from './validate/ValidateRules.js';
import {textField} from './fields/Fields';
import {bindActionCreators} from 'redux';
import * as PageActions from '../../actions/PageActions.js';

class Task extends Component {

  constructor(props) {
    super(props);
    this.state = {
      taskId: null
    };
  }

  componentDidMount() {
    const {taskId, tasks} = this.props;
    let taskEditId;
    if (taskId) {
      const task = {
        ...tasks.items.filter(task => task.id === taskId)[0]
      };
      taskEditId = taskId;
      //change form inputs
      this.props.dispatch(change('save-task-form', 'name', task.name));
      this.props.dispatch(change('save-task-form', 'size', task.size));
      this.props.dispatch(change('save-task-form', 'tag', task.tag));
    } else {
      taskEditId = Math.max.apply(Math, tasks.items.map(task => task.id)) + 1;
    }

    this.setState({taskId: taskEditId});
  }

  @autobind
  submitEvent(data) {
    const task = {
      id: this.state.taskId,
      ...data
    }
    this.props.actions.editTask(task);
    this.props.hideForm();
  }

  render() {
    const {handleSubmit, pristine, reset, submitting} = this.props;

    return (
      <div className="container-fluid">
        <div className="col-md-12">
          <h1>Add/Edit Form</h1>
          <form id="editTaskForm" noValidate>
            <div className="row">
              <div className="col-md-12">
                <div className="form-group">
                  <Field type="text" name="name" id="name" className="form-control" component={textField} placeholder="Name" label="Name" validate={[validateRules.required, validateRules.minValue3]}/>
                  <Field type="number" name="size" id="size" className="form-control" component={textField} placeholder="Size" label="Size" validate={[validateRules.required, validateRules.number]}/>
                  <Field type="text" name="tag" id="tag" className="form-control" component={textField} placeholder="Tag" label="Tag" validate={[validateRules.required, validateRules.minValue3]}/>
                </div>
              </div>
            </div>
            <div className="row form-buttons">
              <div className="col-md-6">
                <button type="button" className="btn btn-outline btn-success col-md-12" onClick={handleSubmit(this.submitEvent)}>Save</button>
              </div>
              <div className="col-md-6">
                <button type="button" className="btn btn-outline btn-danger col-md-12" onClick={this.props.hideForm}>Cancel</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

Task.propTypes = {
  taskId: React.PropTypes.number,
  hideForm: React.PropTypes.func
}

Task = reduxForm({
  form: 'save-task-form',
  initialValues: {
    name: '',
    size: 0
  }
})(Task);

function mapStateToProps(state) {
  return {tasks: state.rootReducer.page.tasks};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(PageActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Task);
