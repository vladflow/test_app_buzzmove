import React from 'react';

export const textField = (field) => {
  let fieldError = '';
  let fieldClass = field.className;
  if (field.meta.touched && field.meta.error) {
    fieldClass += ' error_field';
    fieldError = <span className="text-field-error">{field.meta.error}</span>;
  }
  return (
    <div className="fieldContainer">
      <label htmlFor={field.input.name}>{field.label}</label>
      <input {...field.input} type={field.type} id={field.id} className={fieldClass} placeholder={field.placeholder}/> {fieldError}
    </div>
  )
}
