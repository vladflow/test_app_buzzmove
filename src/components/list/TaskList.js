import React, {Component} from 'react';
import autobind from 'autobind-decorator';
import Task from '../forms/Task';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as PageActions from '../../actions/PageActions.js';

class TaskList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showForm: false,
      currentTask: null
    };
  }

  @autobind
  filterList(e, tag = false) {
    e.preventDefault();
    this.props.actions.filterBy(tag);
  }

  @autobind
  showForm(e, id) {
    e.preventDefault();

    this.setState({showForm: true, currentTask: id});
  }

  @autobind
  hideForm() {
    this.setState({showForm: false, currentTask: null});
  }

  @autobind
  removeItem(e, id) {
    e.preventDefault();
    this.props.actions.removeTask(id);
  }

  render() {
    const {tasks} = this.props;

    const filterItems = tasks.tags.map((tag, index) => <li key={index + 1}><a href="#" onClick={(e) => this.filterList(e, tag)}>To {tag}</a></li>);
    filterItems.unshift(<li key="0" onClick={(e) => this.filterList(e)}><a href="#">To All</a></li>);

    const items = tasks.items.length > 0
      ? tasks.items.filter(element => !tasks.filterBy || element.tag === tasks.filterBy).map((element, index) => {
        return (
          <tr key={index} className="item-tr">
            <td className="first-td">{element.name}</td>
            <td>{element.size} ft&sup3;</td>
            <td className="job-td">
              <span>{`${element.tag}`}</span>
            </td>
            <td className="buttons-td">
              <div className="center-block text-center">
                <div className="btn-group pull-center">
                  <div className="btn-group">
                    <button type="button" className="btn btn-warning btn-outline dropdown-toggle col-md-12" data-toggle="dropdown">
                      Actions
                      <span className="caret"></span>
                    </button>
                    <ul className="dropdown-menu" role="menu">
                      <li>
                        <a href="#" onClick={(e) => this.showForm(e, element.id)}>Edit survey</a>
                      </li>
                      <li>
                        <a href="#" onClick={(e) => this.removeItem(e, element.id)}>Delete survey</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        )
      })
      : <tr>
        <td colSpan="4">Items not found</td>
      </tr>;
    return !this.state.showForm
      ? (
        <div className="container-fluid no-padding-margin">
          <div className="row">
            <div className="col-md-12 no-padding-margin">
              <table className="table table-condensed TasksTable">
                <thead>
                  <tr>
                    <th colSpan="3"></th>
                    <th>
                      <a href="#" className="col-sm-12 btn btn-warning btn-outline dropdown-toggle pull-right" onClick={(e) => this.showForm(e)}>
                        New
                      </a>
                    </th>
                  </tr>
                  <tr>
                    <th width="30%" className="title-th">Tasks</th>
                    <th width="20%"></th>
                    <th width="25%" className="buttons-th">
                      <div className="form-group">
                        <div className="col-sm-12 select-col">
                          <div className="center-block text-center">
                            <div className="btn-group pull-center">
                              <div className="btn-group">
                                <button type="button" className="btn btn-primary btn-outline dropdown-toggle col-md-12" data-toggle="dropdown">
                                  To {!tasks.filterBy ? 'All' : tasks.filterBy}
                                  <span className="caret"></span>
                                </button>
                                <ul className="dropdown-menu" role="menu">
                                  {filterItems}
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </th>
                    <th width="25%" className="buttons-th">
                      <div className="form-group">
                        <div className="col-sm-12 select-col">
                          <div className="center-block text-center">
                            <div className="btn-group pull-center">
                              <div className="btn-group">
                                <button type="button" className="btn btn-primary btn-outline dropdown-toggle col-md-12" data-toggle="dropdown">
                                  Sort By
                                  <span className="caret"></span>
                                </button>
                                <ul className="dropdown-menu" role="menu">
                                  <li>
                                    <a href="#">Name</a>
                                  </li>
                                  <li>
                                    <a href="#">Size</a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {items}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      )
      : <Task hideForm={this.hideForm} taskId={this.state.currentTask}/>
  }
}

function mapStateToProps(state) {
  return {tasks: state.rootReducer.page.tasks, pageStatus: state.rootReducer.page.page_status};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(PageActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
