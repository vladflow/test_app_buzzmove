import React, {Component} from 'react';
import {connect} from 'react-redux';

class PageLoader extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {isFetching, loaded, loadingError, errorMessage} = this.props.pageStatus;

    let content = null;
    if (!isFetching && loaded) {
      content = this.props.children;
    } else if (isFetching) {
      content = (
        <div className="page-loader"></div>
      );
    } else if (loadingError) {
      content = (
        <div className="page-error">{errorMessage}</div>
      );
    }

    return content;
  }
}
function mapStateToProps(state) {
  return {pageStatus: state.rootReducer.page.page_status};
}

export default connect(mapStateToProps)(PageLoader);
